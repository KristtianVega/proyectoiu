import { crearTagInItem,msClose,editTags,crearItem,getFormStr,reset,showItems,showWelcome,showTags,editItem,crearTags} from "./eventos.js";

//Verificamos la ubicacion del usuario
const URLactual = document.location; 
if (URLactual.pathname == "/create.html" || URLactual.pathname == "/index.html" ){
    reset()
    //Leemos el formulario
    const formLog = document.getElementById("form-log"); 
    //Optenemos los datos del formulario al hacer submit
    formLog.addEventListener("submit",getFormStr);
}else{
    showWelcome();

    const close = document.getElementById("close")
    close.addEventListener("click",msClose);
    
    const items = document.getElementById("items");
    items.addEventListener("click",showItems);

    const tags = document.getElementById("tags");
    tags.addEventListener("click",showTags);

    const formItem = document.getElementById("formItem");
    formItem.addEventListener("submit",crearItem)

    const formTag = document.getElementById("formTag");
    formTag.addEventListener("submit",crearTags)

    const formItemEd = document.getElementById("formItemEd");
    formItemEd.addEventListener("submit",editItem)

    const formTagEd = document.getElementById("formTagEd");
    formTagEd.addEventListener("submit",editTags)

    const AddTag= document.getElementById("AddTag")
    AddTag.addEventListener("submit",crearTagInItem)
}

