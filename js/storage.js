//Modificacion de datos almacenados en el LocalStorage
import { msCreado, searchUser, msExiste, searchID, IID,msExtag,msTagAdd } from "./eventos.js";

const usersWrite = (dataForm) => {
    //Leemos el local storage
    const userRead = usersRead("Users");
    const dataRead = usersRead("Data");

    //Genero un numero aleatorio de 5 digitos
    var ranID = Date.now() % 100000

    //Verifico si ya hay un json
    if (userRead == null) {
        const usuarios = {
            users: [
                {
                    Username: dataForm.username,
                    Password: dataForm.password,
                    Nombre: dataForm.regName,
                    Id: ranID
                },
            ]
        };
        //Se guarda del dato en el localStotage
        localStorage.setItem("Users", JSON.stringify(usuarios));

        const userDatas = {
            data: [
                {
                    Id: ranID,
                    lstTags: [],
                    lstItems: [],
                },
            ]
        };
        //Se guarda del dato en el localStotage
        localStorage.setItem("Data", JSON.stringify(userDatas));
        //Se informa al usuario
        msCreado()

    } else {
        //Verifico si el usuario ya existe
        var existe = searchUser(dataForm, userRead)
        if (existe == false) {
            //Si no existe se crea
            const user = {
                Username: dataForm.username,
                Password: dataForm.password,
                Nombre: dataForm.regName,
                Id: ranID
            };
            userRead.users.push(user);
            //Se guarda del dato en el localStotage
            localStorage.setItem("Users", JSON.stringify(userRead));
            //Se informa al usuario
            const userData={
                Id: ranID,
                lstTags: [],
                lstItems: [],
            }
            dataRead.data.push(userData)
            localStorage.setItem("Data", JSON.stringify(dataRead));
            msCreado()
        } else {
            msExiste()
        }
    }
}

const dataRead = (id) => {
    //Leemos el local storage
    const docRead = usersRead("Data");
    var dataUser = searchID(id, docRead);
    return dataUser
}

const usersRead = (doc) => {
    //Buscamos la clave en el local storage y retornamos el objeto
    const data = JSON.parse(localStorage.getItem(doc));
    return data
}

const SetActual = (user) => {
    //Se guarda el Id actual en el LS
    localStorage.setItem("User", JSON.stringify(user));
}

const GetActual = () => {
    //Se obtiene el Id actual del LS
    var user = JSON.parse(localStorage.getItem("User"));
    return user
}

const writeItem = (dataForm) => {
    const docRead = usersRead("Data");
    const {Id}= GetActual();
    
    //Genero un numero aleatorio de 5 digitos
    var ranID = Date.now() % 100000
    //Creo el objeto con los datos
    const items = {
        Titulo: dataForm.title,
        Username: dataForm.userN,
        Password: dataForm.pass,
        URL: dataForm.url,
        Comentario: dataForm.comen,
        IID: ranID,
        Tags: []
    };
    console.log(ranID);
    const newData = docRead.data.map(userData => {
        if (userData.Id==Id){
            userData.lstItems.push(items)
            return userData
        }else{
            return userData
        }
    })
    docRead.data = newData;
    //Se guarda del dato en el localStotage
    localStorage.setItem("Data", JSON.stringify(docRead));
    
    return ranID
}

const writeTagInItem = (dataForm) => {
    const docRead = usersRead("Data");
    const {Id}= GetActual();
    //Creo el objeto con los datos
    const tag = dataForm.Tag;
    console.log("item;",IID);
    const newData = docRead.data.map(userData => {
        if (userData.Id==Id){
            console.log(userData);
            userData.lstItems = userData.lstItems.map(item=>{
                if (item.IID==IID) {
                        if (item.Tags.indexOf(tag)===-1) {
                            item.Tags.push(tag);
                            msTagAdd()
                        }else{
                            msExtag()
                        }                   
                          
                    return item
                }else{
                    return item
                }
            })
            return userData
        }else{
            return userData
        }
    })
    console.log("copi",newData);
    docRead.data = newData;
    //Se guarda del dato en el localStotage
    localStorage.setItem("Data", JSON.stringify(docRead));
}

const writeItemEd = (dataForm) => {
    const user=GetActual()
    const IDU = user.Id
    const docRead = usersRead("Data");
    const newData = docRead.data.map(userData => {
        if (userData.Id==IDU){
            userData.lstItems = userData.lstItems.map(item=>{
                if (item.IID==IID) {
                    return {...dataForm,IID,Tags:item.Tags}
                }else{
                    return item
                }
            })
            return userData
        }else{
            return userData
        }
    })
    docRead.data = newData;
    //Se guarda del dato en el localStotage
    localStorage.setItem("Data", JSON.stringify(docRead));
}

const rmvItemD = (Id) =>{
    const user=GetActual()
    const IDU = user.Id
    const docRead = usersRead("Data");
    const newData = docRead.data.map(userData => {
        if (userData.Id==IDU){
            userData.lstItems = userData.lstItems.filter(item=>{
                return item.IID !=Id
            })
            return userData
        }else{
            return userData
        }
    })
    docRead.data = newData;
    //Se guarda del dato en el localStotage
    localStorage.setItem("Data", JSON.stringify(docRead));
    
}

const writeTag = (dataForm) =>{
    const docRead = usersRead("Data");
    const {Id}= GetActual();
    var ranID = Date.now() % 100000
    //Creo el objeto con los datos
    const tag = {
        Nombre: dataForm.tagName,
        TID:ranID,
    };
    
    const newData = docRead.data.map(userData => {
        if (userData.Id==Id){
            userData.lstTags.push(tag)
            return userData
        }else{
            return userData
        }
    })
    docRead.data = newData;
    //Se guarda del dato en el localStotage
    localStorage.setItem("Data", JSON.stringify(docRead));
}

const rmvTagD = (Id) =>{
    const user=GetActual()
    const IDU = user.Id
    const docRead = usersRead("Data");
    const newData = docRead.data.map(userData => {
        if (userData.Id==IDU){
            userData.lstTags = userData.lstTags.filter(item=>{
                return item.TID !=Id
            })
            return userData
        }else{
            return userData
        }
    })

    docRead.data = newData;
    //Se guarda del dato en el localStotage
    localStorage.setItem("Data", JSON.stringify(docRead));
    
}

const writeTagEd = (dataForm) =>{
    const user=GetActual()
    const IDU = user.Id
    const TID = IID;
    const docRead = usersRead("Data");
    const newData = docRead.data.map(userData => {
        if (userData.Id==IDU){
            userData.lstTags = userData.lstTags.map(item=>{
                if (item.TID==TID) {
                    return {...dataForm,TID }
                }else{
                    return item
                }
            })
            return userData
        }else{
            return userData
        }
    })
    docRead.data = newData;
    //Se guarda del dato en el localStotage
    localStorage.setItem("Data", JSON.stringify(docRead));
}

const searchTagInItem = (TID) => {
    
    const docRead = usersRead("Data");
    const user=GetActual()
    const IDU = user.Id
    let lstItems=[];
    docRead.data.forEach(userData => {
        if (userData.Id==IDU) {
            console.log(userData.Id,IDU);
            userData.lstItems.forEach(item=>{
                
                item.Tags.forEach(tag=>{
                    console.log(tag,TID);

                    if (tag.TID==TID) {
                        
                        lstItems.push(item)

                    }

                })
            
            })
        }
        
    });
    return(lstItems);
}
export{searchTagInItem}
export {writeTagEd}
export{rmvTagD}
export {writeTag}
export {writeItemEd}
export {rmvItemD}
export { writeItem}
export { dataRead }
export { GetActual }
export { SetActual }
export { usersRead }
export { usersWrite }
export{writeTagInItem}