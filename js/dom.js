//Modificar el DOM
import { showCrear, showCTag, ItemToMod, whoItem, IID, TagToMod, getDatos,GetActual } from "./eventos.js";
import { searchTagInItem }from "./storage.js";
let lastD = "";
const changeContainer = (datos, op) => {
    const data = document.getElementById("data");
    const divMenu = document.getElementById("divMenu");
    const btCrear = document.createElement("button");
    const btEliminar = document.createElement("button");
    const btEditar = document.createElement("button");
    const divScroll = document.createElement("div");
    const btCtag = document.createElement("button");
    const btEtag = document.createElement("button");
    const btEdtag = document.createElement("button");
    const divBt = document.createElement("div");
    const h1 = data.getElementsByTagName('h1')[0];

    switch (op) {
        case 1: //Bienvenida
            data.getElementsByTagName('p')[0].textContent = "Seleccione el menu al que desea ingresar";
            h1.textContent = `Bienvenido "${datos.Nombre}"`
            break;
        case 2://Menu de items
            divMenu.innerHTML = "";
            //Cambiamos el titulo
            h1.removeAttribute("id")
            h1.textContent = `Menu de Items`;
            //Cambiamos el contenido del parrafo
            data.getElementsByTagName('p')[0].textContent = "";
            //Creamos el scroll
            divScroll.classList.add("scroll")
            divScroll.id = "divScroll"
            divMenu.appendChild(divScroll)
            //Creamos los botones
            btCrear.innerText = "Crear Item";
            btEliminar.innerText = "Eliminar Item"
            btEliminar.id = "btEliminar"
            btEliminar.disabled = true
            btEditar.innerText = "Editar Item"
            btEditar.id = "btEditar"
            btEditar.disabled = true

            divBt.appendChild(btCrear);
            divBt.appendChild(btEliminar);
            divBt.appendChild(btEditar)
            divBt.id = "divBt";
            divMenu.appendChild(divBt);

            //Creamos los listener
            btCrear.addEventListener("click", showCrear)
            break;
        case 3://Menu de tags
            divMenu.innerHTML = "";
            //Cambiamos el titulo
            h1.removeAttribute("id")
            h1.textContent = `Menu de Tags`;
            //Cambiamos el contenido del parrafo
            data.getElementsByTagName('p')[0].textContent = "";
            //Creamos el scroll
            divScroll.classList.add("scroll")
            divScroll.id = "divScroll"
            divMenu.appendChild(divScroll)
            //Creamos los botones
            btCtag.innerText = "Crear Tag";
            btEtag.innerText = "Eliminar Tag"
            btEdtag.innerText = "Editar Tag"
            btEtag.id = "btEtag"
            btEdtag.id = "btEdtag"
            btEtag.disabled = true
            btEdtag.disabled = true
            divBt.appendChild(btCtag);
            divBt.appendChild(btEtag);
            divBt.appendChild(btEdtag)
            divBt.id = "divBt";
            divMenu.appendChild(divBt);
            //Creamos los listener
            btCtag.addEventListener("click", showCTag)
        case 4://Sin datos
            data.getElementsByTagName('p')[0].textContent = "Sin datos";
            break
        case 5://Mostrar items
            //Obtengo el div
            data.getElementsByTagName('p')[0].textContent = "";
            const item = document.getElementById("divScroll")
            const Eliminar = document.getElementById("btEliminar")
            const Editar = document.getElementById("btEditar")
            //Vacio el div
            item.innerHTML = "";
            lastD = datos;
            //Recorro la lista de items
            Array.from(datos).forEach(dato => {
                //Creo un div
                const div = document.createElement("div")
                for (const key in dato) {
                    const p = document.createElement("p")
                    p.setAttribute("datID", dato.IID)
                    p.textContent = `${key}: ${dato[key]}`
                    if (key == "Username" || key == "Password") {
                        p.style.display = "none";
                        p.name = "inActivo";
                    }
                    div.appendChild(p)
                }
                //Agrego el texto al hijo
                div.setAttribute("datID", dato.IID)
                const childP = div.childNodes;
                let cont = 0
                //Creo el listener para saber que item es
                div.addEventListener("click", (e) => {
                    if (cont == 0) {
                        ItemToMod(whoItem(e));
                        cont++
                    }
                    Eliminar.disabled = false;
                    Editar.disabled = false;
                    childP.forEach(child => {
                        if (child.name == "inActivo") {
                            child.style.display = "block";
                            child.name = "activo";
                        } else if (child.name == "activo") {
                            child.style.display = "none";
                            child.name = "inActivo";
                        }
                    });
                });
                item.appendChild(div)
            });
            break
        case 6://Mostrar tags
            data.getElementsByTagName('p')[0].textContent = "";
            //Obtengo el div
            const tag = document.getElementById("divScroll")
            const EleTag = document.getElementById("btEtag")
            const EdTag = document.getElementById("btEdtag")
            //Vacio el div
            tag.innerHTML = "";
            lastD = datos;
            //Recorro la lista de tags
            Array.from(datos).forEach(dato => {
                //Creo un div
                const divItems = document.createElement("divItems")

                const div = document.createElement("div")
                for (const key in dato) {
                    const p = document.createElement("p")
                    p.setAttribute("datID", dato.TID)
                    p.textContent = `${key}: ${dato[key]}`
                    if (key == "TID"|| key =="Items") {
                        p.style.display = "none";
                        p.name = "inActivo";
                    }
                    div.appendChild(p)
                }

                const lstItems=searchTagInItem(dato.Tag)

                lstItems.forEach(item => {
                    const h4 = document.createElement("h4")
                    h4.innerText="Lista Info Items"
                    const hr = document.createElement("hr")
                    const hr2 = document.createElement("hr")
                    divItems.style.display="none"
                    divItems.name="inActivo";
                    divItems.appendChild(hr2)
                    divItems.appendChild(h4)
                    divItems.appendChild(hr)
                    console.log(item);
                    for (const key in item) {
                        if (key == "IID"|| key =="Titulo"|| key =="URL"|| key =="Username") {
                            const p = document.createElement("p")
                            p.textContent = `${key}: ${item[key]}`
                            divItems.appendChild(p)
                        }
                    }
                    div.appendChild(divItems)
                });
                //Agrego el texto al hijo
                div.setAttribute("datID", dato.TID)
                const childP = div.childNodes;
                //Creo el listener para saber que item es
                let cont = 0;
                div.addEventListener("click", (e) => {
                    if (cont == 0) {
                        TagToMod(whoItem(e));
                        cont++
                    }
                    EleTag.disabled = false;
                    EdTag.disabled = false;
                    childP.forEach(child => {
                        if (child.name == "inActivo") {
                            child.style.display = "block";
                            child.name = "activo";
                        } else if (child.name == "activo") {
                            child.style.display = "none";
                            child.name = "inActivo";
                        }
                    });
                });
                tag.appendChild(div)
            });
            break
    }
}

const changePop = (op) => {
    switch (op) {
        case 0:
            const Titulo = document.getElementById("Titulo")
            const Username = document.getElementById("Username")
            const Password = document.getElementById("Password")
            const Comentario = document.getElementById("Comentario")
            const URL = document.getElementById("URL")

            lastD.map(item => {
                if (item.IID == IID) {
                    Titulo.value = item.Titulo
                    Username.value = item.Username
                    Password.value = item.Password
                    Comentario.value = item.Comentario
                    URL.value = item.URL

                }
            })

            break;
        case 1:
            const Nombre = document.getElementById("Nombre")
            lastD.map(item => {
                if (item.TID == IID) {
                    Nombre.value = item.Nombre
                }
            })
            break;
        case 2:
            var user = GetActual();
            var datos = getDatos(user.Id)
            if (datos) {
                if (datos.lstTags.length != 0) {
                    console.log(datos.lstTags);
                    const lst = document.getElementById("lst");
                    lst.innerHTML="";
                    datos.lstTags.forEach(tag=>{
                        for (const key in tag) {
                            const p = document.createElement("p")
                            p.textContent = `${tag[key]}`
                            console.log(tag[key]);
                            if (key == "TID") {
                                p.style.display = "none";
                                p.name = "inActivo";
                            }
                            lst.appendChild(p)
                        };
                    })
                   
                };
            };
            break;
    }
}


export { changePop }
export { changeContainer }