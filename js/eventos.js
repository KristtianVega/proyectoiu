//Manejo de datos
import { writeTagInItem, writeTagEd, rmvTagD, writeTag, writeItemEd, usersWrite, usersRead, SetActual, GetActual, dataRead, writeItem, rmvItemD } from "./storage.js";
import { changeContainer, changePop } from "./dom.js";
let op = 0;
let IID = 0;

const getFormStr = (e) => {
    e.preventDefault();//quito los valores por defecto
    const input = new FormData(e.target).entries()
    //Optenemos el objeto con los datos ingresados
    const dataForm = Object.fromEntries(input)
    //Buscamos los datos
    submitData(dataForm)
}

const submitData = (dataForm) => {
    //Si el objeto tiene 3 claves se esta registrando
    if ((Object.keys(dataForm).length) == 3) {
        //Se crea el usuario
        usersWrite(dataForm);
    } else {
        let Users = usersRead("Users")
        if (Users) {
            //Se busca el usuario en el localStorage
            var existe = searchUser(dataForm, usersRead("Users"));
            if (existe == true) {
                //corroboramos los datos ingresados con los datos en el Localstorage
                var user = confirmID(dataForm, usersRead("Users"));
                if (user != null) {
                    welcome()
                    SetActual(user)
                }
            } else {
                msNoExiste();
            }
        } else {
            msNoExiste();
        }

    }
}

const msCreado = () => {
    window.alert("Usuario Creado con exito");
    window.location.href = "./index.html"
}

const msExiste = () => {
    window.alert("El usuario ya existe");
    window.location.href = "./create.html"
}

const msNoExiste = () => {
    window.alert("El usuario no existe");
    window.location.href = "./index.html"
}

const msCItem = () => {
    window.alert("Item Creado");
}

const msTItem = () => {
    window.alert("Tag Creado");
}

const msExtag = () => {
    window.alert("El tag ya existe");
}

const msClose = () => {
    if (window.confirm(`¿Desea cerrar sesion?`)){
        window.location.href = "./index.html"
    }
}

const msTagAdd = () => {
    window.alert("Tag Agregado");
}

const welcome = () => {
    window.location.href = "./items.html"
}

const searchUser = (dataForm, docRead) => {
    var existe = false;
    //Verificamos si ya existe el usuario
    //Recorremos el objeto
    for (let i = 0; i < docRead.users.length; i++) {
        if (docRead.users[i].Username == dataForm.username) {
            existe = true;
            break;
        }
    }
    return existe
}

const confirmID = (dataForm, docRead) => {
    var user = null;
    //Verificamos si los datos coinciden
    //Recorremos el objeto
    for (let i = 0; i < docRead.users.length; i++) {
        if (docRead.users[i].Username == dataForm.username && docRead.users[i].Password == dataForm.password) {
            user = docRead.users[i];
            break;
        }
    }
    return user
}

const reset = () => {
    //Re reinicia el ID dek usuario actual
    SetActual(null)
}

const showWelcome = () => {
    if (op != 1) {
        //Mensaje de bienvenida con el nombre del usuario 
        op = 1;
        var user = GetActual();
        changeContainer(user, op)
    }
}

const showItems = () => {
    // miro el usuario actual 
    var user = GetActual();
    //Verifico si el container ya existe
    if (op != 2) {
        op = 2;
        // muestro la interfaz del menu
        changeContainer(user, op)
    }
    //obtengo los datos del usuario
    var datos = getDatos(user.Id)

    //Verifico si el usuario si tiene objetos en los datos
    if (datos) {
        if (datos.lstItems.length != 0) {

            changeContainer(datos.lstItems, 5)
        } else {
            changeContainer(null, 4);
        }
    } else {
        changeContainer(null, 4);
    }
}

const showTags = () => {
    // miro el usuario actual 
    var user = GetActual();
    //Verifico si el container ya existe
    if (op != 3) {
        op = 3;
        // muestro la interfaz del menu
        changeContainer(user, op)
    }
    //obtengo los datos del usuario
    var datos = getDatos(user.Id)

    //Verifico si el usuario si tiene objetos en los datos
    if (datos) {
        if (datos.lstTags.length != 0) {
            changeContainer(datos.lstTags, 6)
        } else {
            changeContainer(null, 4);
        }
    } else {
        changeContainer(null, 4);
    }
}

//Obtengo los datos
const getDatos = (id) => {
    return dataRead(id)
}

const searchID = (id, docRead) => {
    //Se crea una variable con la que se guardan los datos
    var data = null;
    for (let i = 0; i < docRead.data.length; i++) {
        if (docRead.data[i].Id == id) {
            data = docRead.data[i];
            break;
        }
    }
    return data
}

const showCrear = () => {
    const crearItem = document.getElementById("crearItem");
    crearItem.style.left = "0";
    crearItem.style.opacity = "1";
    crearItem.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(0);
}

const showEd = () => {
    const EdItem = document.getElementById("EdItem");
    EdItem.style.left = "0";
    EdItem.style.opacity = "1";
    EdItem.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(2);
}

const showCTag = () => {
    const crearTag = document.getElementById("crearTag");
    crearTag.style.left = "0";
    crearTag.style.opacity = "1";
    crearTag.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(1);
}

const showEdTag = () => {
    const EdTag = document.getElementById("EdTag");
    EdTag.style.left = "0";
    EdTag.style.opacity = "1";
    EdTag.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(3);
}

const showAddTag=()=>{
    const AddTag = document.getElementById("AddTag");
    AddTag.style.left = "0";
    AddTag.style.opacity = "1";
    AddTag.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(4);
}

const btDshow = (num) => {
    const btAtras = document.getElementsByClassName("btAtras")[num];
    btAtras.addEventListener("click", () => { dontShow(num) })
}

const dontShow = (num) => {
    switch (num) {
        case 0:
            const crearItem = document.getElementById("crearItem");
            crearItem.style.left = "-100%";
            crearItem.style.opacity = "0";
            crearItem.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showItems();
            break;
        case 1:
            const crearTag = document.getElementById("crearTag");
            crearTag.style.left = "-100%";
            crearTag.style.opacity = "0";
            crearTag.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showTags();
            break;
        case 2:
            const EdItem = document.getElementById("EdItem");
            EdItem.style.left = "-100%";
            EdItem.style.opacity = "0";
            EdItem.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showItems();
            break;
        case 3:
            const EdTag = document.getElementById("EdTag");
            EdTag.style.left = "-100%";
            EdTag.style.opacity = "0";
            EdTag.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showTags();
            break;
        case 4:
            const AddTag = document.getElementById("AddTag");
            AddTag.style.left = "-100%";
            AddTag.style.opacity = "0";
            AddTag.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showItems();
            break;
    }
}

const getForm = (e) => {
    e.preventDefault();//quito los valores por defecto
    const input = new FormData(e.target).entries()
    //Optenemos el objeto con los datos ingresados
    const dataForm = Object.fromEntries(input)
    //se retorna el objeto
    return dataForm
}

//ITEMS
const crearItem = (e) => {
    IID=writeItem(getForm(e));
    e.target.reset();
    msCItem();
    dontShow(0)
    changePop(2)
    showAddTag()
}

const crearTagInItem = (e)=>{
    writeTagInItem(getForm(e));
    e.target.reset();
    dontShow(4)
}

const editItem = (e) => {
    writeItemEd(getForm(e));
    e.target.reset();
    msCItem();
    dontShow(2)
    changePop(2)
    showAddTag()
}

//Se busca el Id del item
const whoItem = (e) => {
    const item = e.target.getAttribute("datID");
    return item
}

//Se modifica el item encontrado
const ItemToMod = (idItem) => {
    const btEliminar = document.getElementById("btEliminar")
    btEliminar.addEventListener("click", () => {
        if (window.confirm(`¿Desea eliminar el Item ${idItem}?`)) {
            if (idItem) {
                rmvItemD(idItem)
                showTags()
                showItems()
            }
        }
    })
    const btEditar = document.getElementById("btEditar")
    btEditar.addEventListener("click", () => {
        showEd();
        IID = idItem;
        changePop(0);

    })
}
//TAGs
const crearTags = (e) => {
    writeTag(getForm(e));
    e.target.reset();
    msTItem();
}

const editTags = (e) => {
    writeTagEd(getForm(e));
    e.target.reset();
    msCItem();
}

const TagToMod = (idTag) => {
    const btEtag = document.getElementById("btEtag")
    btEtag.addEventListener("click", () => {
        if (idTag) {
            if (window.confirm(`¿Desea eliminar el tag ${idTag}?`)) {
                rmvTagD(idTag)
                showTags()
            }
        }
    })
    const btEdtag = document.getElementById("btEdtag")
    btEdtag.addEventListener("click", () => {
        showEdTag();
        IID = idTag;
        changePop(1);
    })
}

export { editTags }
export { TagToMod }
export { crearTags }
export { IID }
export { editItem }
export { showEd }
export { whoItem }
export { ItemToMod }
export { crearItem }
export { getForm }
export { showCTag }
export { showCrear }
export { searchID }
export { showTags }
export { showWelcome }
export { showItems }
export { reset }
export { searchUser }
export { msExiste }
export { msCreado }
export { getFormStr }
export { msClose}
export {getDatos}
export {GetActual}
export {crearTagInItem}
export{msExtag}
export{msTagAdd}
